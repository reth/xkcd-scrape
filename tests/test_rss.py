import unittest
from xkcdscrape import xkcd

class Tests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Tests, self).__init__(*args, **kwargs)  # that's a mess
        self.archive = xkcd.parseArchive()
    
    def test_for_working(self):
        item = xkcd.getLastRSS()
        self.assertIsInstance(item, str)

if __name__ == "__main__":
    unittest.main()